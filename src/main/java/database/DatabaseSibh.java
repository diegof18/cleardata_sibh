package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseSibh {

	public Connection conectar() {
		
        String driver = "org.postgresql.Driver";
        String user = "postgres";
        String senha = "TC0ntr0l";
        String url = "jdbc:postgresql://143.107.108.110:5432/hidrologia";

        try {
            Class.forName(driver);
            System.out.println(driver);
            Connection con = (Connection) DriverManager.getConnection(url, user, senha);
            return con;
        } catch (ClassNotFoundException ex) {
            System.err.print(ex.getMessage());
        } catch (SQLException e) {
            System.err.print(e.getMessage());
        }
        return null;
	}

	public boolean limparMedicoesDesartaveis(int dias) {
		
		System.out.println("Limpando medições com mais de " + dias + " dia(s) de vida...");
		
		String sql = "delete from dados_temp_loggers where id not in (select max(id) from dados_temp_loggers " + 
				"where prefixo IN(select distinct prefixo from dados_temp_loggers) " + 
				"and origem_da_informacao = 'WSJAVA-SAISP' group by prefixo) and sync_status = 1 and data_hora < now() - Interval '"+dias+" days'";
		
		
		Connection connection = conectar();
		
		if(connection != null) {
			try {
				
				Statement smt = connection.createStatement();
				
				int linhasRemovidas = smt.executeUpdate(sql);
				
				System.out.println("Medições Removidas -" + linhasRemovidas);
				
				smt.close();
				return true;
				
			} catch(Exception e) {
				System.err.println(e);
			}

		}
		
		fecharConec(connection);
		
		return false;
		
	}
	
	private static void fecharConec(Connection connection) {
		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
}
