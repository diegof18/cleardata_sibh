package controllers;

import database.DatabaseSibh;
import interfaces.CleardataInterface;

public class ClearDataController implements CleardataInterface{

	public boolean limparTabelaDadosTempLoggers() {
		
		DatabaseSibh database = new DatabaseSibh();
		
		database.limparMedicoesDesartaveis(2);
		
		return true;
	}
	
	
}
