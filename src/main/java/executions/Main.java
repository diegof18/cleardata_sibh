package executions;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import controllers.ClearDataController;

public class Main {

	static Timer timer_a;
	
	public static void main(String[] args) {
		
		System.out.println("Iniciado.");
		
		TimerTask attDadosDia = new TimerTask() {

		@Override
		public void run() {
			
			System.out.println("Iniciando processo de limpeza da base..");
			
			ClearDataController controller = new ClearDataController();
			
			if(controller.limparTabelaDadosTempLoggers()) {
				System.out.println("Processo concluído.");
			} else {
				System.out.println("Erro ao executar processo.");
			}
			
		}
		
	};
	
	
	Calendar today = Calendar.getInstance();
	today.set(Calendar.HOUR_OF_DAY, 23);
	today.set(Calendar.MINUTE, 50);
	today.set(Calendar.SECOND, 00);
	
	timer_a = new Timer();
	
	timer_a.schedule(attDadosDia, today.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS));
		
		

	}

}
